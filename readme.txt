1. Averigüe para que sirve la función “addEventListener” dentro del lenguaje de
programación TypeScript o JavaScript.
	R: Es la forma de registrar un listener de eventos, como se especifica en W3C DOM.
Sus beneficios son los siguientes: Permite agregar mas de un listener a un solo evento.
2. Averigüe como se crea un vector en TypeScript y JavaScript, y realice un ejemplo de cada
uno; además de indicar como podemos saber la longitud de un vector.
	R: const mixedTypedArray = [100, true, 'freeCodeCamp', {}];
	   const element = array[index];

4. Cuál es la diferencia entre JavaScript y TypeScript; y crea una variable, que almacene un
número y luego hacer que se muestre en la consola en los dos lenguajes. Para ello, crear un
archivo ts y js.
	R: TypeScript es un lenguaje de desarrollo de JavaScript de la era moderna, mientras que
	JavaScript es un lenguaje de secuencias de comandos que ayuda a crear páginas web interactivas.